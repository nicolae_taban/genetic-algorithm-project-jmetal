package problem;

import org.uma.jmetal.problem.AbstractGenericProblem;
import org.uma.jmetal.solution.integersolution.IntegerSolution;
import org.uma.jmetal.solution.integersolution.impl.DefaultIntegerSolution;
import org.uma.jmetal.util.bounds.Bounds;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("serial")
public class MicroprocessorOptimizationProblem extends AbstractGenericProblem<IntegerSolution> {
    final private List<Bounds<Integer>> integerBounds;
    final private String simulatorPath;
    final private String resultsPath;
    final private String configFolderPath;
    final private List<String> fileContents;
    final private List<String> variablePlaceholders;
    final private String partialResultsPath;
    FileWriter partialResultsWriter;
    final private Object mutex;
    AtomicInteger evaluationNumber;
    final String benchmark;

    public MicroprocessorOptimizationProblem(int numberOfVariables, List<Bounds<Integer>> bounds, List<String> variablePlaceholders, String simulatorPath, String benchmark) {
        this.benchmark = benchmark;
        this.simulatorPath = simulatorPath;
        this.resultsPath = simulatorPath + "/results";
        if (!Files.exists(Paths.get(resultsPath))) {
            try {
                Files.createDirectory(Paths.get(resultsPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.partialResultsPath = simulatorPath + "/partial_results/";
        if (!Files.exists(Paths.get(partialResultsPath))) {
            try {
                Files.createDirectory(Paths.get(partialResultsPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            partialResultsWriter = new FileWriter(partialResultsPath + "partial_results.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        mutex = new Object();

        this.evaluationNumber = new AtomicInteger(0);
        this.variablePlaceholders = variablePlaceholders;
        setNumberOfVariables(numberOfVariables);
        setNumberOfObjectives(2);
        setNumberOfConstraints(0);
        setName("IntegerProblem");
        integerBounds = bounds;
        this.configFolderPath = simulatorPath + "/config/";
        String genericConfigFilePath = simulatorPath + "/config/gainestownmod.cfg";
        fileContents = new ArrayList<>();

        if (Files.exists(Paths.get(genericConfigFilePath))) {
            Scanner scanner = null;
            try {
                scanner = new Scanner(new File(genericConfigFilePath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            while (scanner.hasNextLine()) {
                fileContents.add(scanner.nextLine());
            }
        } else {
            // throw new Exception("Could not find the generic");
        }
    }

    /**
     * Evaluate() method
     */
    public IntegerSolution evaluate(IntegerSolution solution) {
        List<String> concreteFile = new ArrayList<>();


        for (int i = 0; i < fileContents.size(); i++) {
            String line = fileContents.get(i);
            String finalLine = fileContents.get(i);
            if (line.contains("{") && line.contains("}")) {
                String placeholder = line.substring(line.indexOf('{'), line.indexOf('}') + 1);
                int positionInChromosome = -1;
                for (int j = 0; j < variablePlaceholders.size(); j++) {
                    if (variablePlaceholders.get(j).equals(placeholder)) {
                        positionInChromosome = j;
                        break;
                    }
                }

                Integer outputValue = solution.getVariable(positionInChromosome);
                outputValue = (int) Math.pow(2, outputValue);
                finalLine = finalLine.replace(placeholder, outputValue.toString());
            }
            concreteFile.add(finalLine);
        }

        int evaluation = evaluationNumber.getAndIncrement();
        String configFileName = configFolderPath + evaluation + ".cfg";
        try {
            FileWriter myWriter = new FileWriter(configFileName);
            for (int i = 0; i < concreteFile.size(); i++) {
                myWriter.write(concreteFile.get(i));
                myWriter.write('\n');
            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred when writing to file.");
            e.printStackTrace();
        }
        // finished writing the file

        StringBuilder partialResult = new StringBuilder("Individual " + evaluation + ";");

        for (int i = 0; i < solution.getNumberOfVariables(); i++) {
            partialResult.append(solution.getVariable(i)).append(";");
        }

        String resultsDirectory = resultsPath + "/" + evaluation;
        try {
            Files.createDirectory(Paths.get(resultsDirectory));
        } catch (IOException e) {
            e.printStackTrace();
        }
        // get number of cores
        int numberOfCoresPosition = -1;
        for (int i = 0; i < solution.getNumberOfVariables(); i++) {
            if (variablePlaceholders.get(i).equals("{numberOfCores}")) {
                numberOfCoresPosition = i;
                break;
            }
        }
        Integer numberOfCores = solution.getVariable(numberOfCoresPosition);
        numberOfCores = (int) Math.pow(2, numberOfCores);

        ProcessBuilder pb = new ProcessBuilder("/bin/bash", simulatorPath + "/lerun_test.sh", simulatorPath, resultsDirectory, numberOfCores.toString(), configFileName, benchmark);
        Process process = null;
        try {
            process = pb.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = "";
            StringBuilder output = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                //output.append(line).append("\n");
                System.out.println(line);
            }
            process.waitFor();
            process.destroy();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        Scanner energyScanner = null;
        String consoleOutputPath = resultsDirectory + "/" + "console_output.txt";
        Double energy = null;
        try {
            if (Files.exists(Paths.get(consoleOutputPath))) {
                energyScanner = new Scanner(new File(consoleOutputPath));
                String line = "";
                while (energyScanner.hasNextLine()) {
                    line = energyScanner.nextLine();
                    if (!line.contains("total")) {
                        continue;
                    }
                    line = line.trim();
                    String[] split = line.split("\\s+");
                    try {
                        energy = Double.parseDouble(split[3]);
                    } catch (NumberFormatException ignored) {
                    }
                    break;
                }
                energyScanner.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (energy != null) {
            System.out.println("Good energy: " + energy + " on iteration " + evaluation);
            solution.setObjective(0, energy);
            partialResult.append("Energy: ").append(energy).append(";");
        } else {
            System.out.println("Bad energy on iteration " + evaluation);
            double wrongEnergy = JMetalRandom.getInstance().nextDouble() + 5;
            partialResult.append("Energy ").append(wrongEnergy).append(";").append("Wrong;");
            solution.setObjective(0, wrongEnergy);
            File errorFile = new File(resultsDirectory + "/" + "FISHY_ENERGY.txt");
            try {
                errorFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Scanner cpiScanner = null;
        Double cpi = null;
        String simOutPath = resultsDirectory + "/" + "sim.out";
        try {
            if (Files.exists(Paths.get(simOutPath))) {
                cpiScanner = new Scanner(new File(simOutPath));
                String line = null;
                while (cpiScanner.hasNextLine()) {
                    line = cpiScanner.nextLine();
                    if (!line.contains("IPC")) {
                        continue;
                    }
                    line = line.trim();
                    double totalIPC = 0d;
                    String[] split = line.split("\\s+");
                    for (int i = 0; i < split.length; i++) {
                        if (tryParseDouble(split[i]) != null) {
                            totalIPC += tryParseDouble(split[i]);
                        }
                    }
                    if (totalIPC != 0) {
                        cpi = 1 / totalIPC;
                    }
                    break;
                }
                cpiScanner.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (cpi != null) {
            System.out.println("Good CPI: " + cpi + " on iteration " + evaluation);
            partialResult.append("CPI: ").append(cpi).append(";");
            solution.setObjective(1, cpi);
        } else {
            double wrongCPI = JMetalRandom.getInstance().nextDouble() + 5;
            solution.setObjective(1, wrongCPI);
            partialResult.append("CPI ").append(wrongCPI).append(";").append("Wrong;");
            System.out.println("Bad CPI:" + wrongCPI + " on iteration " + evaluation);
            File errorFile = new File(resultsDirectory + "/" + "FISHY_CPI.txt");
            try {
                errorFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        partialResult.append("\n");
        synchronized (mutex) {
            try {
                partialResultsWriter.write(partialResult.toString());
                partialResultsWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return solution;
    }

    public IntegerSolution createSolution() {
        return new DefaultIntegerSolution(getNumberOfObjectives(), getNumberOfConstraints(), integerBounds);
    }

    Double tryParseDouble(String value) {
        try {
            double result;
            result = Double.parseDouble(value);
            return result;
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
