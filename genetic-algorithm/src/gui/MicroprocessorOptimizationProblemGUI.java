package gui;

import worker.OptimizationWorker;

import javax.swing.*;
import java.io.File;

public class MicroprocessorOptimizationProblemGUI {
    private JPanel rootPanel;
    private JLabel populationSizeLabel;
    private JLabel numberOfGenerationsLabel;
    private JLabel crossoverPercentageLabel;
    private JLabel mutationPercentageLabel;
    private JLabel simulatorPathLabel;
    private JLabel benchmarkLabel;
    private JLabel crossoverOperatorLabel;
    private JLabel mutationOperatorLabel;

    private JTextField populationSizeTextField;
    private JTextField simulatorPathTextField;
    private JTextField numberOfGenerationsTextField;
    private JTextField crossoverPercentageTextField;
    private JTextField mutationPercentageTextField;

    private JButton browseButton;
    private JButton runAlgorithmButton;
    private JComboBox crossoverOperatorComboBox;

    private JComboBox mutationOperatorComboBox;
    private JComboBox benchmarkComboBox;


    private OptimizationWorker worker;

    public Integer populationSize;
    public Integer numberOfGenerations;
    public Integer crossoverPercentage;
    public Integer mutationPercentage;

    public MicroprocessorOptimizationProblemGUI() {
        String[] benchmarks = new String[]{
                "splash2-barnes",
                "splash2-cholesky",
                "splash2-fft",
                "splash2-fmm",
                "splash2-lu.cont",
                "splash2-lu.ncont",
                "splash2-ocean.cont",
                "splash2-ocean.ncont",
                "splash2-radix",
                "splash2-raytrace",
                "splash2-water.nsq",
                "splash2-water.sp"
        };
        benchmarkComboBox.addItem("Barnes");
        benchmarkComboBox.addItem("Cholesky");
        benchmarkComboBox.addItem("Fast Fourier Transform");
        benchmarkComboBox.addItem("fmm");
        benchmarkComboBox.addItem("lu.cont factorization");
        benchmarkComboBox.addItem("lu.ncont factorization");
        benchmarkComboBox.addItem("ocean.cont");
        benchmarkComboBox.addItem("ocean.ncont");
        benchmarkComboBox.addItem("Radix Sort");
        benchmarkComboBox.addItem("Raytracing");
        benchmarkComboBox.addItem("Water NSQ");
        benchmarkComboBox.addItem("Water SP");
        benchmarkComboBox.setSelectedIndex(0);

        crossoverOperatorComboBox.addItem("Integer SBX");
        crossoverOperatorComboBox.addItem("Single Point Crossover");
        crossoverOperatorComboBox.setSelectedIndex(0);

        mutationOperatorComboBox.addItem("Polynomial Mutation");
        mutationOperatorComboBox.addItem("Simple Gene Mutation");
        mutationOperatorComboBox.setSelectedIndex(0);

        worker = null;
        simulatorPathTextField.setText(System.getProperty("user.dir") + "/sniper");
        browseButton.addActionListener(e -> {
            JFileChooser fc = new JFileChooser();
            fc.setCurrentDirectory(new java.io.File(".")); // start at application current directory
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnVal = fc.showSaveDialog(rootPanel);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                File simulatorFolder = fc.getSelectedFile();
                String path = simulatorFolder.getAbsolutePath();
                simulatorPathTextField.setText(path);
            }
        });
        runAlgorithmButton.addActionListener(e -> {
            if (!new File(simulatorPathTextField.getText()).exists()){
                displayError("The simulator folder does not exist! Retry!");
                return;
            }
            String simulatorPath = simulatorPathTextField.getText();

            populationSize = tryParseInt(populationSizeTextField.getText());
            if (populationSize == null) {
                String message = "Could not parse the population size! Retry!";
                displayError(message);
                return;
            } else if (populationSize < 2) {
                String message = "The population size is too small! Retry!";
                displayError(message);
                return;
            }

            numberOfGenerations = tryParseInt(numberOfGenerationsTextField.getText());
            if (numberOfGenerations == null) {
                String message = "Could not parse the number of generations! Retry!";
                displayError(message);
                return;
            } else if (numberOfGenerations < 1) {
                String message = "The number of generations is too small! Retry!";
                displayError(message);
                return;
            }

            crossoverPercentage = tryParseInt(crossoverPercentageTextField.getText());
            if (crossoverPercentage == null) {
                String message = "Could not parse the crossover percentage! Retry!";
                displayError(message);
                return;
            } else if (crossoverPercentage < 0 || crossoverPercentage > 100) {
                String message = "The crossover percentage must be between 0 and 100! Retry!";
                displayError(message);
                return;
            }

            mutationPercentage = tryParseInt(mutationPercentageTextField.getText());
            if (mutationPercentage == null) {
                String message = "Could not parse the mutation percentage! Retry!";
                displayError(message);
                return;
            } else if (mutationPercentage < 0 || mutationPercentage > 100) {
                String message = "The mutation percentage must be between 0 and 100! Retry!";
                displayError(message);
                return;
            }


            double crossoverProbability = crossoverPercentage / 100.0;
            double mutationProbability = mutationPercentage / 100.0;

            int crossoverOperator = crossoverOperatorComboBox.getSelectedIndex();
            int mutationOperator = mutationOperatorComboBox.getSelectedIndex();

            String benchmark = benchmarks[benchmarkComboBox.getSelectedIndex()];

            worker = new OptimizationWorker(
                    populationSize,
                    numberOfGenerations * populationSize,
                    crossoverProbability,
                    mutationProbability,
                    simulatorPath,
                    crossoverOperator,
                    mutationOperator,
                    benchmark
            );
            worker.execute();
        });
    }

    public void displayError(String message) {
        JOptionPane.showMessageDialog(new JFrame(), message, "Error",
                JOptionPane.ERROR_MESSAGE);
    }

    Integer tryParseInt(String value) {
        try {
            Integer result;
            result = Integer.parseInt(value);
            return result;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Microprocessor Optimization Problem");
        frame.setContentPane(new MicroprocessorOptimizationProblemGUI().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
