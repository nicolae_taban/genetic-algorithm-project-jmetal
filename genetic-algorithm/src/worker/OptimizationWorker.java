package worker;

import runner.MyRunner;

import javax.swing.*;
import java.util.List;

public class OptimizationWorker extends SwingWorker<List<Object>, List<Object>> {
    int populationSize;
    int offspringPopulationSize;
    int numberOfGenerations;
    double crossoverProbability;
    double mutationProbability;
    String simulatorPath;
    int crossoverOperator;
    int mutationOperator;
    String benchmark;

    public OptimizationWorker(
            int populationSize,
            int numberOfGenerations,
            double crossoverProbability,
            double mutationProbability,
            String simulatorPath,
            int crossoverOperator,
            int mutationOperator,
            String benchmark
    ) {
        this.populationSize = populationSize;
        this.offspringPopulationSize = populationSize;
        this.numberOfGenerations = numberOfGenerations;
        this.crossoverProbability = crossoverProbability;
        this.mutationProbability = mutationProbability;
        this.simulatorPath = simulatorPath;
        this.crossoverOperator = crossoverOperator;
        this.mutationOperator = mutationOperator;
        this.benchmark = benchmark;
    }


    MyRunner runner;
    @Override
    protected List<Object> doInBackground() {
        runner = new MyRunner(
                populationSize,
                offspringPopulationSize,
                numberOfGenerations,
                crossoverProbability,
                mutationProbability,
                simulatorPath,
                crossoverOperator,
                mutationOperator,
                benchmark
        );
        runner.run();
        return null;
    }

    @Override
    protected void done() {
        super.done();
    }
}
