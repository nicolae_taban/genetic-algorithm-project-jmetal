package runner;

import org.uma.jmetal.experimental.componentbasedalgorithm.algorithm.multiobjective.nsgaii.NSGAII;
import org.uma.jmetal.experimental.componentbasedalgorithm.catalogue.evaluation.impl.MultithreadedEvaluation;
import org.uma.jmetal.experimental.componentbasedalgorithm.catalogue.termination.Termination;
import org.uma.jmetal.experimental.componentbasedalgorithm.catalogue.termination.impl.TerminationByEvaluations;
import org.uma.jmetal.operator.crossover.CrossoverOperator;
import org.uma.jmetal.operator.crossover.impl.IntegerNPointCrossover;
import org.uma.jmetal.operator.crossover.impl.IntegerSBXCrossover;
import org.uma.jmetal.operator.mutation.MutationOperator;
import org.uma.jmetal.operator.mutation.impl.IntegerPolynomialMutation;
import org.uma.jmetal.operator.mutation.impl.IntegerSimpleRandomMutation;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.doublesolution.DoubleSolution;
import org.uma.jmetal.solution.integersolution.IntegerSolution;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.bounds.Bounds;
import org.uma.jmetal.util.fileoutput.SolutionListOutput;
import org.uma.jmetal.util.fileoutput.impl.DefaultFileOutputContext;
import org.uma.jmetal.util.observer.impl.EvaluationObserver;
import org.uma.jmetal.util.observer.impl.RunTimeChartObserver;
import problem.MicroprocessorOptimizationProblem;

import java.util.ArrayList;
import java.util.List;

public class MyRunner {
    int populationSize;
    int offspringPopulationSize;
    int numberOfGenerations;
    double crossoverProbability;
    double mutationProbability;
    String simulatorPath;
    int crossoverOperator;
    int mutationOperator;
    String benchmark;

    public MyRunner(
            int populationSize,
            int offspringPopulationSize,
            int numberOfGenerations,
            double crossoverProbability,
            double mutationProbability,
            String simulatorPath,
            int crossoverOperator,
            int mutationOperator,
            String benchmark
    ) {
        this.populationSize = populationSize;
        this.offspringPopulationSize = offspringPopulationSize;
        this.numberOfGenerations = numberOfGenerations;
        this.crossoverProbability = crossoverProbability;
        this.mutationProbability = mutationProbability;
        this.simulatorPath = simulatorPath;
        this.crossoverOperator = crossoverOperator;
        this.mutationOperator = mutationOperator;
        this.benchmark = benchmark;
    }

    public void run() {
        Problem<IntegerSolution> problem;
        NSGAII<IntegerSolution> algorithm;
        CrossoverOperator<IntegerSolution> crossover;
        MutationOperator<IntegerSolution> mutation;

        int numberOfVariables = 11;
        List<Bounds<Integer>> integerBounds = new ArrayList<>(numberOfVariables);
        List<String> variablePlaceholders = new ArrayList<>();

        // number of cores
        integerBounds.add(Bounds.create(0, 2));
        variablePlaceholders.add("{numberOfCores}");

        // number of logical CPUs
        integerBounds.add(Bounds.create(0, 2));
        variablePlaceholders.add("{logicalCPUS}");

        // dispatch width
        integerBounds.add(Bounds.create(1, 4));
        variablePlaceholders.add("{dispatchWidth}");

        // window size
        integerBounds.add(Bounds.create(5, 9));
        variablePlaceholders.add("{windowSize}");

        // outstanding load stores
        integerBounds.add(Bounds.create(2, 4));
        variablePlaceholders.add("{loadStore}");

        // l1 icache size
        integerBounds.add(Bounds.create(6, 10));
        variablePlaceholders.add("{l1iSize}");

        // l1 icache assoc
        integerBounds.add(Bounds.create(0, 4));
        variablePlaceholders.add("{l1iAssociativity}");

        // l1 dcache size
        integerBounds.add(Bounds.create(6, 10));
        variablePlaceholders.add("{l1dSize}");

        // l1 dcache assoc
        integerBounds.add(Bounds.create(0, 4));
        variablePlaceholders.add("{l1dAssociativity}");

        // l2 size
        integerBounds.add(Bounds.create(6, 10));
        variablePlaceholders.add("{l2Size}");

        // l2 assoc
        integerBounds.add(Bounds.create(0, 4));
        variablePlaceholders.add("{l2Associativity}");

        problem = new MicroprocessorOptimizationProblem(numberOfVariables, integerBounds, variablePlaceholders, simulatorPath, benchmark);

        double crossoverProbability = this.crossoverProbability;
        double crossoverDistributionIndex = 20.0;

        if (crossoverOperator == 0) {
            crossover = new IntegerSBXCrossover(crossoverProbability, crossoverDistributionIndex);
        } else {
            crossover = new IntegerNPointCrossover(crossoverProbability, 1);
        }

        double mutationProbability = this.mutationProbability;
        double mutationDistributionIndex = 20.0;
        if (mutationOperator == 0) {
            mutation = new IntegerPolynomialMutation(mutationProbability, mutationDistributionIndex);
        } else {
            mutation = new IntegerSimpleRandomMutation(this.mutationProbability);
        }

        int populationSize = this.populationSize;
        int offspringPopulationSize = this.offspringPopulationSize;

        Termination termination = new TerminationByEvaluations(this.numberOfGenerations);

        algorithm =
                (NSGAII<IntegerSolution>) new NSGAII<>(
                        problem, populationSize, offspringPopulationSize, crossover, mutation, termination)
                        .withEvaluation(new MultithreadedEvaluation<>(4, problem));

        EvaluationObserver evaluationObserver = new EvaluationObserver(1000);
        RunTimeChartObserver<DoubleSolution> runTimeChartObserver =
                new RunTimeChartObserver<>("NSGA-II", 5000, null);

        algorithm.getObservable().register(evaluationObserver);
        algorithm.getObservable().register(runTimeChartObserver);

        algorithm.run();

        new SolutionListOutput(algorithm.getResult())
                .setVarFileOutputContext(new DefaultFileOutputContext("VAR.csv", ","))
                .setFunFileOutputContext(new DefaultFileOutputContext("FUN.csv", ","))
                .print();

        JMetalLogger.logger.info("Objectives values have been written to file FUN.csv");
        JMetalLogger.logger.info("Variables values have been written to file VAR.csv");
    }
}
